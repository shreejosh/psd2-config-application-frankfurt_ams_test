management:
  security: null
  basic:
    enabled: false
  context-path: /
logging.level.org.springframework:
  web: ERROR
  context: ERROR
app:
  payloadLog: true
  sentDetailErrorMessage: true
  sendErrorPayload: true
  excludeProxyUrls: pfadmin.webservices.com,config.webservices.com,runtime.webservices.com
  istlsMAEnabled: true
domainName: https://api-frankfurttest.apiboidev.com/1/api/open-banking/v2.0
jwt:
  secret: my-very-secret-key
  issuer: OpenBanking Ltd
spring:
  application:
    name: dynamicclientv2
    profile: boiTest
wellKnownConfig:
  active: false
  wellKnowEndpoint: https://auth-test.apibank-cma2plus.in/oauth/as/.well-known/openid-configuration
  issuer: https://api-frankfurttest.apiboidev.com
  scopes: 
   - openid
   - payments
   - accounts
  response_types_supported:
     - code id_token
  grant_types_supported:
      - authorization_code
      - refresh_token
      - client_credentials
  id_token_signing_alg_values_supported:
      - PS256
  token_endpoint_auth_methods_supported:
      - tls_client_auth
  request_object_signing_alg_values_supported:
      - PS256
pfclientreg:
  prefix: https://pfadmin.webservices.com:9999/pf-admin-api
  clientsUrl: /v1/oauth/clients
  clientByIdUrl: /v1/oauth/clients/{clientId}
  caImportUrl: /v1/certificates/ca/import
  replicateUrl: /v1/cluster/replicate
  adminuser: ApiAdmin
  secretType: CERTIFICATE
  certIssuer: Trust Any
  tokenMgrRefId: INTREFATM
  idTokenSigningAlgorithm: PS256
  policyGroupId: oidcpol
  restrictedResponseTypes:
    - code id_token
  refreshRolling: DONT_ROLL
  persistentGrantExpirationType: SERVER_DEFAULT
  persistentGrantExpirationTimeUnit: DAYS
  persistentGrantExpirationTime: 0
  grantsandscopes:
    AISP:
      granttypes:
        - IMPLICIT
        - CLIENT_CREDENTIALS
        - AUTHORIZATION_CODE
        - REFRESH_TOKEN
      scopes:
        - accounts
        - openid
    PISP:
      granttypes:
        - IMPLICIT
        - CLIENT_CREDENTIALS
        - AUTHORIZATION_CODE
        - REFRESH_TOKEN
      scopes:
        - payments
        - openid
muleclientReg:
     createContractForAllApis: false
     isContractRequired: false
openbanking:
     jwks.endpoint: https://keystore.openbankingtest.org.uk/keystore/openbanking.jwks
     scim.attributes: urn:openbanking:organisation:1.0:status
     tokenUrl: https://matls-sso.openbankingtest.org.uk/as/token.oauth2
     apiUrl: https://matls-api.openbankingtest.org.uk/scim/v2/OBThirdPartyProviders/
     clientAssertionType: urn:ietf:params:oauth:client-assertion-type:jwt-bearer
     grantType: client_credentials
     scope: TPPReadAll
     keyId: dKnFuBkszgvi2r2nLdU4ldewc_s
     clientId: 7dkUc8BjHPYgSadbvt2Ysr
     keyAlias: signingtpp
     transportkeyAlias: transporttpp
     jwtTokenValidity: 1800000
     tokenIssuer: http://tpp.com
ldap:
  config:
    url: ldaps://ds.webservices.com:636
    userDn: cn=appUserReadWriteEnhanced,ou=applicationusers,ou=people,dc=boi,dc=co.uk,dc=capgeminibank,dc=com
  tppapplication:
    basedn: ou=clients,ou=application,dc=boi,dc=co.uk,dc=capgeminibank,dc=com
    clientdn: clientId=
  tppgroup:
    basedn: ou=tpp,ou=groups,dc=boi,dc=co.uk,dc=capgeminibank,dc=com
  clientAppgroup:
    basedn: ou=clientAppMapping,ou=groups,dc=boi,dc=co.uk,dc=capgeminibank,dc=com
eureka:
  client:
    registryFetchIntervalSeconds: 10
    serviceUrl:
      defaultZone: https://eureka-eu-central-1a.webservices.com:8761/eureka/
  instance:
    secure-port: 0
    secure-port-enabled: true
    lease-expiration-duration-in-seconds: 10
    lease-renewal-interval-in-seconds: 10
    metadataMap:
      instanceId: ${spring.application.name}:${spring.application.instance_id:${random.value}}
server:
  port: 0
  ssl:
    enabled: true
    key-store: classpath:${keyStoreName}
    protocol: TLSv1.2
    ciphers:
       - TLS_DHE_RSA_WITH_AES_128_GCM_SHA256
       - TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
       - TLS_DHE_RSA_WITH_AES_256_GCM_SHA384
       - TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
dynamicClient: on